﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace view
{
    public partial class frmPrincipal : Form
    {
        public frmPrincipal()
        {
            InitializeComponent();
        }

        private void frmPrincipal_Load(object sender, EventArgs e)
        {

        }

        private void btnCadAlunos_Click(object sender, EventArgs e)
        {
            frmCadAlunos cadAlunos = new frmCadAlunos();
            cadAlunos.MdiParent = this;
            cadAlunos.Show();
        }

        private void cadastroDeAlunosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnCadAlunos_Click(sender, e);
        }

        private void lançamentoDasNotasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnCadastrarNotas_Click(sender, e);
        }

        private void btnCadastrarNotas_Click(object sender, EventArgs e)
        {
            frmCadNotas cadNotas = new frmCadNotas();
            cadNotas.MdiParent = this;
            cadNotas.Show();
        }

        private void btnCalcularNota_Click(object sender, EventArgs e)
        {
            frmCalcularNota calcularNota = new frmCalcularNota();
            calcularNota.MdiParent = this;
            calcularNota.Show();
        }

        private void calculoDasMédiasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnCalcularNota_Click(sender, e);
        }
    }
}
