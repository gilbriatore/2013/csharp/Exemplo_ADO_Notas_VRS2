﻿namespace view
{
    partial class frmLocalizarAluno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grpDados = new System.Windows.Forms.GroupBox();
            this.btnLocalizar = new System.Windows.Forms.Button();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.lblNome = new System.Windows.Forms.Label();
            this.grpResultados = new System.Windows.Forms.GroupBox();
            this.grdAlunos = new System.Windows.Forms.DataGridView();
            this.grdId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdNome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdCpf = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdContato = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.grpDados.SuspendLayout();
            this.grpResultados.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdAlunos)).BeginInit();
            this.SuspendLayout();
            // 
            // grpDados
            // 
            this.grpDados.Controls.Add(this.btnLocalizar);
            this.grpDados.Controls.Add(this.txtNome);
            this.grpDados.Controls.Add(this.lblNome);
            this.grpDados.Location = new System.Drawing.Point(12, 12);
            this.grpDados.Name = "grpDados";
            this.grpDados.Size = new System.Drawing.Size(492, 88);
            this.grpDados.TabIndex = 0;
            this.grpDados.TabStop = false;
            this.grpDados.Text = "Dados da consulta";
            // 
            // btnLocalizar
            // 
            this.btnLocalizar.Location = new System.Drawing.Point(392, 40);
            this.btnLocalizar.Name = "btnLocalizar";
            this.btnLocalizar.Size = new System.Drawing.Size(91, 28);
            this.btnLocalizar.TabIndex = 6;
            this.btnLocalizar.Text = "&Localizar";
            this.btnLocalizar.UseVisualStyleBackColor = true;
            this.btnLocalizar.Click += new System.EventHandler(this.btnLocalizar_Click);
            // 
            // txtNome
            // 
            this.txtNome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNome.Location = new System.Drawing.Point(12, 44);
            this.txtNome.MaxLength = 60;
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(372, 20);
            this.txtNome.TabIndex = 5;
            // 
            // lblNome
            // 
            this.lblNome.Location = new System.Drawing.Point(8, 24);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(140, 23);
            this.lblNome.TabIndex = 4;
            this.lblNome.Text = "Informe uma parte do nome";
            // 
            // grpResultados
            // 
            this.grpResultados.Controls.Add(this.grdAlunos);
            this.grpResultados.Location = new System.Drawing.Point(12, 112);
            this.grpResultados.Name = "grpResultados";
            this.grpResultados.Size = new System.Drawing.Size(492, 196);
            this.grpResultados.TabIndex = 1;
            this.grpResultados.TabStop = false;
            this.grpResultados.Text = "Resultados da consulta";
            // 
            // grdAlunos
            // 
            this.grdAlunos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdAlunos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.grdId,
            this.grdNome,
            this.grdCpf,
            this.grdContato});
            this.grdAlunos.Location = new System.Drawing.Point(8, 20);
            this.grdAlunos.MultiSelect = false;
            this.grdAlunos.Name = "grdAlunos";
            this.grdAlunos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdAlunos.Size = new System.Drawing.Size(472, 164);
            this.grdAlunos.TabIndex = 0;
            this.grdAlunos.TabStop = false;
            this.grdAlunos.DoubleClick += new System.EventHandler(this.grdAlunos_DoubleClick);
            // 
            // grdId
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.grdId.DefaultCellStyle = dataGridViewCellStyle1;
            this.grdId.HeaderText = "ID Aluno";
            this.grdId.Name = "grdId";
            this.grdId.ReadOnly = true;
            this.grdId.Width = 60;
            // 
            // grdNome
            // 
            this.grdNome.HeaderText = "Nome";
            this.grdNome.Name = "grdNome";
            this.grdNome.ReadOnly = true;
            this.grdNome.Width = 150;
            // 
            // grdCpf
            // 
            dataGridViewCellStyle2.Format = "000\\.000\\.000\\-00";
            dataGridViewCellStyle2.NullValue = null;
            this.grdCpf.DefaultCellStyle = dataGridViewCellStyle2;
            this.grdCpf.HeaderText = "CPF";
            this.grdCpf.Name = "grdCpf";
            this.grdCpf.ReadOnly = true;
            // 
            // grdContato
            // 
            dataGridViewCellStyle3.Format = "\\(00\\) 0000\\-0000";
            this.grdContato.DefaultCellStyle = dataGridViewCellStyle3;
            this.grdContato.HeaderText = "Contato";
            this.grdContato.Name = "grdContato";
            this.grdContato.ReadOnly = true;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(308, 316);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(92, 28);
            this.btnOK.TabIndex = 2;
            this.btnOK.Text = "&OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(412, 316);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(92, 28);
            this.btnCancelar.TabIndex = 3;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // frmLocalizarAluno
            // 
            this.AcceptButton = this.btnLocalizar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(517, 361);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.grpResultados);
            this.Controls.Add(this.grpDados);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmLocalizarAluno";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Localizar Alunos";
            this.grpDados.ResumeLayout(false);
            this.grpDados.PerformLayout();
            this.grpResultados.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdAlunos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpDados;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.Button btnLocalizar;
        private System.Windows.Forms.GroupBox grpResultados;
        private System.Windows.Forms.DataGridView grdAlunos;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdId;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdNome;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdCpf;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdContato;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancelar;
    }
}