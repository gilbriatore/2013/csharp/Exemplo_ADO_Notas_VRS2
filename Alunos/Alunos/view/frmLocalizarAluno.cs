﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using dao;
using vo;
namespace view
{
    public partial class frmLocalizarAluno : Form
    {
        frmCadAlunos f;
        frmCadNotas f1;
        frmCalcularNota f2;
        public frmLocalizarAluno(Form form)
        {
            InitializeComponent();
            if (form.Name.ToString() == "frmCadAlunos")
            {
                f = (frmCadAlunos)form;
            }
            else
            {
                if (form.Name.ToString() == "frmCadNotas")
                {
                    f1 = (frmCadNotas)form;
                }
                else
                {
                    if (form.Name.ToString() == "frmCalcularNota")
                    {
                        f2 = (frmCalcularNota)form;
                    }
                }
            }            
        }

        private void btnLocalizar_Click(object sender, EventArgs e)
        {
            Aluno aluno = new Aluno(txtNome.Text);
            string contato;
            grdAlunos.Rows.Clear();
            Application.UseWaitCursor = true;
            foreach (Aluno x in AlunoDAO.obterAlunos(aluno))
            {
                if (x.contato.Trim() == "")
                {
                    contato = "0000000000";
                }
                else
                {
                    contato = x.contato;
                }
                grdAlunos.Rows.Add(x.id, x.nome, Convert.ToInt64(x.cpf), Convert.ToInt64(contato));
            }
            Application.UseWaitCursor = false;
        }

        private void grdAlunos_DoubleClick(object sender, EventArgs e)
        {
            Aluno aluno = new Aluno();
            if (f != null)
            {
                try
                {
                    aluno.id = int.Parse(grdAlunos.CurrentRow.Cells["grdId"].Value.ToString());
                    aluno.nome = grdAlunos.CurrentRow.Cells["grdNome"].Value.ToString();
                    aluno.cpf = grdAlunos.CurrentRow.Cells["grdCpf"].Value.ToString();
                    aluno.contato = grdAlunos.CurrentRow.Cells["grdContato"].Value.ToString();
                    f.txtId.Text = aluno.id.ToString();
                    f.txtNome.Text = aluno.nome;
                    f.mskCpf.Text = aluno.cpf;
                    f.mskContato.Text = aluno.contato;
                    Close();
                }
                catch (Exception error)
                {
                    MessageBox.Show("Aluno inválido", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            if (f1 != null)
            {
                try
                {
                    aluno.id = int.Parse(grdAlunos.CurrentRow.Cells["grdId"].Value.ToString());
                    aluno.nome = grdAlunos.CurrentRow.Cells["grdNome"].Value.ToString();
                    f1.txtAlunoId.Text = aluno.id.ToString(); ;
                    f1.txtAlunoNome.Text = aluno.nome;
                    Close();
                }
                catch (Exception error)
                {
                    MessageBox.Show("Aluno inválido", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            if (f2 != null)
            {
                try
                {
                    aluno.id = int.Parse(grdAlunos.CurrentRow.Cells["grdId"].Value.ToString());
                    aluno.nome = grdAlunos.CurrentRow.Cells["grdNome"].Value.ToString();
                    f2.txtAlunoId.Text = aluno.id.ToString(); ;
                    f2.txtAlunoNome.Text = aluno.nome;
                    Close();
                }
                catch (Exception error)
                {
                    MessageBox.Show("Aluno inválido", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            grdAlunos_DoubleClick(sender, e);
        }
    }
}
