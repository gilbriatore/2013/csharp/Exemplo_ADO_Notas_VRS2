﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using dao;
using vo;

namespace Alunos
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Aluno aluno = new Aluno();
            aluno.nome = "Claudio";
            if (AlunoDAO.incluirAluno(aluno))
            {
                MessageBox.Show("OK");
            }
            else
            {
                MessageBox.Show("NÃO");
            }
        }
    }
}
