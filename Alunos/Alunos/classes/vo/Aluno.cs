﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace vo
{
    class Aluno
    {
        public int id { get; set; }
        public string nome { get; set; }
        public string cpf { get; set; }
        public string contato { get; set; }

        public Aluno() { }
        public Aluno(string nome)
        {
            this.nome = nome;
        }
        public Aluno(int id)
        {
            this.id = id;
        }
        public Aluno(int id, string nome, string cpf, string contato)
        {
            this.id = id;
            this.nome = nome;
            this.cpf = cpf;
            this.contato = contato;
        }
        public Aluno(string nome, string cpf, string contato)
        {
            this.nome = nome;
            this.cpf = cpf;
            this.contato = contato;
        }
        
    }
}
