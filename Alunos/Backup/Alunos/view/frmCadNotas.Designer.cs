﻿namespace view
{
    partial class frmCadNotas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCadNotas));
            this.tbaAlunos = new System.Windows.Forms.ToolStrip();
            this.btnNovo = new System.Windows.Forms.ToolStripButton();
            this.btnLocalizar = new System.Windows.Forms.ToolStripButton();
            this.btnEditar = new System.Windows.Forms.ToolStripButton();
            this.grpDados = new System.Windows.Forms.GroupBox();
            this.txtB4 = new System.Windows.Forms.TextBox();
            this.lblB4 = new System.Windows.Forms.Label();
            this.txtB3 = new System.Windows.Forms.TextBox();
            this.lblB3 = new System.Windows.Forms.Label();
            this.txtB2 = new System.Windows.Forms.TextBox();
            this.lblB2 = new System.Windows.Forms.Label();
            this.txtB1 = new System.Windows.Forms.TextBox();
            this.btnLocAluno = new System.Windows.Forms.Button();
            this.txtAlunoId = new System.Windows.Forms.TextBox();
            this.lblB1 = new System.Windows.Forms.Label();
            this.txtAlunoNome = new System.Windows.Forms.TextBox();
            this.lblAluno = new System.Windows.Forms.Label();
            this.txtId = new System.Windows.Forms.TextBox();
            this.lblId = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.tbaAlunos.SuspendLayout();
            this.grpDados.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbaAlunos
            // 
            this.tbaAlunos.BackColor = System.Drawing.Color.Silver;
            this.tbaAlunos.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNovo,
            this.btnLocalizar,
            this.btnEditar});
            this.tbaAlunos.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.tbaAlunos.Location = new System.Drawing.Point(0, 0);
            this.tbaAlunos.Name = "tbaAlunos";
            this.tbaAlunos.Size = new System.Drawing.Size(630, 25);
            this.tbaAlunos.TabIndex = 2;
            this.tbaAlunos.Text = "toolStrip1";
            // 
            // btnNovo
            // 
            this.btnNovo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnNovo.Image = ((System.Drawing.Image)(resources.GetObject("btnNovo.Image")));
            this.btnNovo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnNovo.Name = "btnNovo";
            this.btnNovo.Size = new System.Drawing.Size(23, 22);
            this.btnNovo.Text = "toolStripButton1";
            this.btnNovo.ToolTipText = "Novo";
            this.btnNovo.Click += new System.EventHandler(this.btnNovo_Click);
            // 
            // btnLocalizar
            // 
            this.btnLocalizar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnLocalizar.Image = ((System.Drawing.Image)(resources.GetObject("btnLocalizar.Image")));
            this.btnLocalizar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnLocalizar.Name = "btnLocalizar";
            this.btnLocalizar.Size = new System.Drawing.Size(23, 22);
            this.btnLocalizar.Text = "toolStripButton1";
            this.btnLocalizar.ToolTipText = "Localizar";
            this.btnLocalizar.Click += new System.EventHandler(this.btnLocalizar_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnEditar.Image = ((System.Drawing.Image)(resources.GetObject("btnEditar.Image")));
            this.btnEditar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(23, 22);
            this.btnEditar.Text = "toolStripButton2";
            this.btnEditar.ToolTipText = "Editar";
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // grpDados
            // 
            this.grpDados.Controls.Add(this.txtB4);
            this.grpDados.Controls.Add(this.lblB4);
            this.grpDados.Controls.Add(this.txtB3);
            this.grpDados.Controls.Add(this.lblB3);
            this.grpDados.Controls.Add(this.txtB2);
            this.grpDados.Controls.Add(this.lblB2);
            this.grpDados.Controls.Add(this.txtB1);
            this.grpDados.Controls.Add(this.btnLocAluno);
            this.grpDados.Controls.Add(this.txtAlunoId);
            this.grpDados.Controls.Add(this.lblB1);
            this.grpDados.Controls.Add(this.txtAlunoNome);
            this.grpDados.Controls.Add(this.lblAluno);
            this.grpDados.Controls.Add(this.txtId);
            this.grpDados.Controls.Add(this.lblId);
            this.grpDados.Enabled = false;
            this.grpDados.Location = new System.Drawing.Point(8, 36);
            this.grpDados.Name = "grpDados";
            this.grpDados.Size = new System.Drawing.Size(608, 120);
            this.grpDados.TabIndex = 3;
            this.grpDados.TabStop = false;
            this.grpDados.Text = "Dados das notas";
            // 
            // txtB4
            // 
            this.txtB4.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtB4.Location = new System.Drawing.Point(236, 76);
            this.txtB4.MaxLength = 60;
            this.txtB4.Name = "txtB4";
            this.txtB4.Size = new System.Drawing.Size(60, 20);
            this.txtB4.TabIndex = 16;
            this.txtB4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblB4
            // 
            this.lblB4.Location = new System.Drawing.Point(172, 80);
            this.lblB4.Name = "lblB4";
            this.lblB4.Size = new System.Drawing.Size(100, 23);
            this.lblB4.TabIndex = 15;
            this.lblB4.Text = "4. Bimestre";
            // 
            // txtB3
            // 
            this.txtB3.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtB3.Location = new System.Drawing.Point(76, 76);
            this.txtB3.MaxLength = 60;
            this.txtB3.Name = "txtB3";
            this.txtB3.Size = new System.Drawing.Size(60, 20);
            this.txtB3.TabIndex = 14;
            this.txtB3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblB3
            // 
            this.lblB3.Location = new System.Drawing.Point(12, 80);
            this.lblB3.Name = "lblB3";
            this.lblB3.Size = new System.Drawing.Size(100, 23);
            this.lblB3.TabIndex = 13;
            this.lblB3.Text = "3. Bimestre";
            // 
            // txtB2
            // 
            this.txtB2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtB2.Location = new System.Drawing.Point(236, 48);
            this.txtB2.MaxLength = 60;
            this.txtB2.Name = "txtB2";
            this.txtB2.Size = new System.Drawing.Size(60, 20);
            this.txtB2.TabIndex = 12;
            this.txtB2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblB2
            // 
            this.lblB2.Location = new System.Drawing.Point(172, 52);
            this.lblB2.Name = "lblB2";
            this.lblB2.Size = new System.Drawing.Size(100, 23);
            this.lblB2.TabIndex = 11;
            this.lblB2.Text = "2. Bimestre";
            // 
            // txtB1
            // 
            this.txtB1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtB1.Location = new System.Drawing.Point(76, 48);
            this.txtB1.MaxLength = 60;
            this.txtB1.Name = "txtB1";
            this.txtB1.Size = new System.Drawing.Size(60, 20);
            this.txtB1.TabIndex = 10;
            this.txtB1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnLocAluno
            // 
            this.btnLocAluno.Image = global::Alunos.Properties.Resources.FIND;
            this.btnLocAluno.Location = new System.Drawing.Point(556, 18);
            this.btnLocAluno.Name = "btnLocAluno";
            this.btnLocAluno.Size = new System.Drawing.Size(40, 23);
            this.btnLocAluno.TabIndex = 9;
            this.btnLocAluno.UseVisualStyleBackColor = true;
            this.btnLocAluno.Click += new System.EventHandler(this.btnLocAluno_Click);
            // 
            // txtAlunoId
            // 
            this.txtAlunoId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAlunoId.Location = new System.Drawing.Point(236, 20);
            this.txtAlunoId.MaxLength = 60;
            this.txtAlunoId.Name = "txtAlunoId";
            this.txtAlunoId.ReadOnly = true;
            this.txtAlunoId.Size = new System.Drawing.Size(60, 20);
            this.txtAlunoId.TabIndex = 8;
            this.txtAlunoId.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblB1
            // 
            this.lblB1.Location = new System.Drawing.Point(12, 52);
            this.lblB1.Name = "lblB1";
            this.lblB1.Size = new System.Drawing.Size(100, 23);
            this.lblB1.TabIndex = 4;
            this.lblB1.Text = "1. Bimestre";
            // 
            // txtAlunoNome
            // 
            this.txtAlunoNome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAlunoNome.Location = new System.Drawing.Point(300, 20);
            this.txtAlunoNome.MaxLength = 60;
            this.txtAlunoNome.Name = "txtAlunoNome";
            this.txtAlunoNome.ReadOnly = true;
            this.txtAlunoNome.Size = new System.Drawing.Size(252, 20);
            this.txtAlunoNome.TabIndex = 3;
            // 
            // lblAluno
            // 
            this.lblAluno.Location = new System.Drawing.Point(172, 24);
            this.lblAluno.Name = "lblAluno";
            this.lblAluno.Size = new System.Drawing.Size(100, 23);
            this.lblAluno.TabIndex = 2;
            this.lblAluno.Text = "Aluno";
            // 
            // txtId
            // 
            this.txtId.Location = new System.Drawing.Point(76, 20);
            this.txtId.Name = "txtId";
            this.txtId.ReadOnly = true;
            this.txtId.Size = new System.Drawing.Size(60, 20);
            this.txtId.TabIndex = 1;
            this.txtId.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblId
            // 
            this.lblId.Location = new System.Drawing.Point(12, 24);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(100, 23);
            this.lblId.TabIndex = 0;
            this.lblId.Text = "ID da nota";
            // 
            // btnCancelar
            // 
            this.btnCancelar.Enabled = false;
            this.btnCancelar.Location = new System.Drawing.Point(540, 164);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 28);
            this.btnCancelar.TabIndex = 5;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnOk
            // 
            this.btnOk.Enabled = false;
            this.btnOk.Location = new System.Drawing.Point(456, 164);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 28);
            this.btnOk.TabIndex = 4;
            this.btnOk.Text = "&OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // frmCadNotas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(630, 210);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.grpDados);
            this.Controls.Add(this.tbaAlunos);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCadNotas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cadastro de Notas";
            this.tbaAlunos.ResumeLayout(false);
            this.tbaAlunos.PerformLayout();
            this.grpDados.ResumeLayout(false);
            this.grpDados.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip tbaAlunos;
        private System.Windows.Forms.ToolStripButton btnNovo;
        private System.Windows.Forms.ToolStripButton btnLocalizar;
        private System.Windows.Forms.ToolStripButton btnEditar;
        public System.Windows.Forms.GroupBox grpDados;
        private System.Windows.Forms.Label lblB1;
        public System.Windows.Forms.TextBox txtAlunoNome;
        private System.Windows.Forms.Label lblAluno;
        public System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.Label lblId;
        public System.Windows.Forms.TextBox txtAlunoId;
        private System.Windows.Forms.Button btnLocAluno;
        public System.Windows.Forms.TextBox txtB1;
        public System.Windows.Forms.TextBox txtB4;
        private System.Windows.Forms.Label lblB4;
        public System.Windows.Forms.TextBox txtB3;
        private System.Windows.Forms.Label lblB3;
        public System.Windows.Forms.TextBox txtB2;
        private System.Windows.Forms.Label lblB2;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnOk;

    }
}