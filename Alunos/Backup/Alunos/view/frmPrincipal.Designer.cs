﻿namespace view
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrincipal));
            this.tbaPrincipal = new System.Windows.Forms.ToolStrip();
            this.btnCadAlunos = new System.Windows.Forms.ToolStripButton();
            this.btnCadastrarNotas = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnCalcularNota = new System.Windows.Forms.ToolStripButton();
            this.mnPrincipal = new System.Windows.Forms.MenuStrip();
            this.cadastrosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastroDeAlunosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lançamentoDasNotasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.calculoDasMédiasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tbaPrincipal.SuspendLayout();
            this.mnPrincipal.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbaPrincipal
            // 
            this.tbaPrincipal.BackColor = System.Drawing.Color.White;
            this.tbaPrincipal.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.tbaPrincipal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnCadAlunos,
            this.btnCadastrarNotas,
            this.toolStripSeparator1,
            this.btnCalcularNota});
            this.tbaPrincipal.Location = new System.Drawing.Point(0, 24);
            this.tbaPrincipal.Name = "tbaPrincipal";
            this.tbaPrincipal.Size = new System.Drawing.Size(761, 39);
            this.tbaPrincipal.TabIndex = 1;
            this.tbaPrincipal.Text = "toolStrip1";
            // 
            // btnCadAlunos
            // 
            this.btnCadAlunos.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCadAlunos.Image = ((System.Drawing.Image)(resources.GetObject("btnCadAlunos.Image")));
            this.btnCadAlunos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCadAlunos.Name = "btnCadAlunos";
            this.btnCadAlunos.Size = new System.Drawing.Size(36, 36);
            this.btnCadAlunos.Text = "toolStripButton1";
            this.btnCadAlunos.ToolTipText = "Cadastro de Alunos";
            this.btnCadAlunos.Click += new System.EventHandler(this.btnCadAlunos_Click);
            // 
            // btnCadastrarNotas
            // 
            this.btnCadastrarNotas.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCadastrarNotas.Image = ((System.Drawing.Image)(resources.GetObject("btnCadastrarNotas.Image")));
            this.btnCadastrarNotas.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCadastrarNotas.Name = "btnCadastrarNotas";
            this.btnCadastrarNotas.Size = new System.Drawing.Size(36, 36);
            this.btnCadastrarNotas.Text = "toolStripButton1";
            this.btnCadastrarNotas.ToolTipText = "Cadastro de Notas";
            this.btnCadastrarNotas.Click += new System.EventHandler(this.btnCadastrarNotas_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 39);
            // 
            // btnCalcularNota
            // 
            this.btnCalcularNota.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnCalcularNota.Image = ((System.Drawing.Image)(resources.GetObject("btnCalcularNota.Image")));
            this.btnCalcularNota.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnCalcularNota.Name = "btnCalcularNota";
            this.btnCalcularNota.Size = new System.Drawing.Size(36, 36);
            this.btnCalcularNota.Text = "Calcular Médias";
            this.btnCalcularNota.Click += new System.EventHandler(this.btnCalcularNota_Click);
            // 
            // mnPrincipal
            // 
            this.mnPrincipal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrosToolStripMenuItem,
            this.notasToolStripMenuItem});
            this.mnPrincipal.Location = new System.Drawing.Point(0, 0);
            this.mnPrincipal.Name = "mnPrincipal";
            this.mnPrincipal.Size = new System.Drawing.Size(761, 24);
            this.mnPrincipal.TabIndex = 3;
            this.mnPrincipal.Text = "menuStrip1";
            // 
            // cadastrosToolStripMenuItem
            // 
            this.cadastrosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastroDeAlunosToolStripMenuItem});
            this.cadastrosToolStripMenuItem.Name = "cadastrosToolStripMenuItem";
            this.cadastrosToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.cadastrosToolStripMenuItem.Text = "&Cadastros";
            // 
            // cadastroDeAlunosToolStripMenuItem
            // 
            this.cadastroDeAlunosToolStripMenuItem.Name = "cadastroDeAlunosToolStripMenuItem";
            this.cadastroDeAlunosToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.cadastroDeAlunosToolStripMenuItem.Size = new System.Drawing.Size(228, 22);
            this.cadastroDeAlunosToolStripMenuItem.Text = "Cadastro de &Alunos...";
            this.cadastroDeAlunosToolStripMenuItem.Click += new System.EventHandler(this.cadastroDeAlunosToolStripMenuItem_Click);
            // 
            // notasToolStripMenuItem
            // 
            this.notasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lançamentoDasNotasToolStripMenuItem,
            this.toolStripMenuItem1,
            this.calculoDasMédiasToolStripMenuItem});
            this.notasToolStripMenuItem.Name = "notasToolStripMenuItem";
            this.notasToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.notasToolStripMenuItem.Text = "&Notas";
            // 
            // lançamentoDasNotasToolStripMenuItem
            // 
            this.lançamentoDasNotasToolStripMenuItem.Name = "lançamentoDasNotasToolStripMenuItem";
            this.lançamentoDasNotasToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this.lançamentoDasNotasToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.lançamentoDasNotasToolStripMenuItem.Text = "&Lançamento das Notas...";
            this.lançamentoDasNotasToolStripMenuItem.Click += new System.EventHandler(this.lançamentoDasNotasToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(241, 6);
            // 
            // calculoDasMédiasToolStripMenuItem
            // 
            this.calculoDasMédiasToolStripMenuItem.Name = "calculoDasMédiasToolStripMenuItem";
            this.calculoDasMédiasToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.U)));
            this.calculoDasMédiasToolStripMenuItem.Size = new System.Drawing.Size(244, 22);
            this.calculoDasMédiasToolStripMenuItem.Text = "Calc&ulo das Médias...";
            this.calculoDasMédiasToolStripMenuItem.Click += new System.EventHandler(this.calculoDasMédiasToolStripMenuItem_Click);
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(761, 488);
            this.Controls.Add(this.tbaPrincipal);
            this.Controls.Add(this.mnPrincipal);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.mnPrincipal;
            this.Name = "frmPrincipal";
            this.Text = "Controle de Notas";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmPrincipal_Load);
            this.tbaPrincipal.ResumeLayout(false);
            this.tbaPrincipal.PerformLayout();
            this.mnPrincipal.ResumeLayout(false);
            this.mnPrincipal.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip tbaPrincipal;
        private System.Windows.Forms.ToolStripButton btnCadAlunos;
        private System.Windows.Forms.ToolStripButton btnCadastrarNotas;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton btnCalcularNota;
        private System.Windows.Forms.MenuStrip mnPrincipal;
        private System.Windows.Forms.ToolStripMenuItem cadastrosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastroDeAlunosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem notasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lançamentoDasNotasToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem calculoDasMédiasToolStripMenuItem;
    }
}