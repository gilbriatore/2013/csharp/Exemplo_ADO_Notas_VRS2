﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using vo;
using bo;
using dao;
namespace view
{
    public partial class frmCalcularNota : Form
    {
        public frmCalcularNota()
        {
            InitializeComponent();
        }

        private void btnLocAluno_Click(object sender, EventArgs e)
        {
            frmLocalizarAluno loc = new frmLocalizarAluno(this);
            loc.ShowDialog();
            if(txtAlunoId.Text != "")
            {
                Aluno aluno = new Aluno(int.Parse(txtAlunoId.Text));
                Nota nota = new Nota();
                nota.aluno = aluno;
                nota = NotaDAO.procurarNotaPorAluno(nota);
                if (nota != null)
                {
                    txtId.Text = nota.id.ToString();
                    txtB1.Text = nota.b1.ToString("N2");
                    txtB2.Text = nota.b2.ToString("N2");
                    txtB3.Text = nota.b3.ToString("N2");
                    txtB4.Text = nota.b4.ToString("N2");
                    txtMedia.Text = NotaBO.calcularMedia(nota).ToString("N2");
                    txtSituacao.Text = NotaBO.verificarSituacao(nota);
                }
                else
                {
                    MessageBox.Show("Não existem notas cadastradas para o aluno selecionado", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }            
        }
    }
}
