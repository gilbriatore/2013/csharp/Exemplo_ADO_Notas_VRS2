﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using dao;
using vo;

namespace view
{
    public partial class frmCadAlunos : Form
    {
        public frmCadAlunos()
        {
            InitializeComponent();
        }

        private void habilitarComponentes()
        {
            grpDados.Enabled = true;
            btnOk.Enabled = true;
            btnCancelar.Enabled = true;
        }

        private void desabilitarComponentes()
        {
            grpDados.Enabled = false;
            btnOk.Enabled = false;
            btnCancelar.Enabled = false;
        }

        private void mostrarAluno(Aluno aluno)
        {
            txtId.Text = aluno.id.ToString();
            txtNome.Text = aluno.nome;
            mskCpf.Text = aluno.cpf;
            mskContato.Text = aluno.contato;
        }

        private void limparCampos()
        {
            foreach (Control c in grpDados.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)(c)).Clear();
                }
                if (c is MaskedTextBox)
                {
                    ((MaskedTextBox)(c)).Clear();
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
           
        }

        private void btnOk_Click(object sender, EventArgs e)
        {

           int id;
            if (txtNome.Text != "" && mskCpf.Text != "")
            {
                if (txtId.Text != "")
                {
                    id = int.Parse(txtId.Text);
                }
                else
                {
                    id = 0;
                }
                Aluno aluno = new Aluno(id, txtNome.Text, mskCpf.Text, mskContato.Text);
                if (aluno.id == 0) //Inclusão
                {
                    if (AlunoDAO.procurarAlunoPorCpf(aluno) == null)
                    {
                        if (AlunoDAO.incluirAluno(aluno) == true)
                        {
                            aluno = AlunoDAO.procurarAlunoPorCpf(aluno);
                            mostrarAluno(aluno);
                            MessageBox.Show("Operação realizada com sucesso", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            desabilitarComponentes();
                        }
                        else
                        {
                            MessageBox.Show("A operação não pôde ser realizada", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Aluno já cadastrado", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                else //Alteração
                {
                    AlunoDAO.atualizarAluno(aluno);
                    MessageBox.Show("Operação realizada com sucesso", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    desabilitarComponentes();
                }
            }
            else
            {
                MessageBox.Show("Os campos Nome e CPF são de preenchimento obrigatório", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            limparCampos();
            desabilitarComponentes();
        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            limparCampos();
            habilitarComponentes();
            txtNome.Focus();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (txtId.Text != "")
            {
                habilitarComponentes();
                txtNome.Focus();
            }            
        }

        private void btnLocalizar_Click(object sender, EventArgs e)
        {
            frmLocalizarAluno loc = new frmLocalizarAluno(this);
            loc.ShowDialog();
        }
    }
}
