﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using vo;
using dao;

namespace view
{
    public partial class frmCadNotas : Form
    {
        public frmCadNotas()
        {
            InitializeComponent();
        }

        private void habilitarComponentes()
        {
            grpDados.Enabled = true;
            btnOk.Enabled = true;
            btnCancelar.Enabled = true;
        }

        private void desabilitarComponentes()
        {
            grpDados.Enabled = false;
            btnOk.Enabled = false;
            btnCancelar.Enabled = false;
        }

        private void mostrarNota(Nota nota)
        {
            txtId.Text = nota.id.ToString();
            txtAlunoId.Text = nota.aluno.id.ToString();
            txtAlunoNome.Text = nota.aluno.nome;
            txtB1.Text = nota.b1.ToString("N2");
            txtB2.Text = nota.b2.ToString("N2");
            txtB3.Text = nota.b3.ToString("N2");
            txtB4.Text = nota.b4.ToString("N2");
        }

        private void limparCampos()
        {
            foreach (Control c in grpDados.Controls)
            {
                if (c is TextBox)
                {
                    ((TextBox)(c)).Clear();
                }
                if (c is MaskedTextBox)
                {
                    ((MaskedTextBox)(c)).Clear();
                }
            }
        }

        private void btnLocAluno_Click(object sender, EventArgs e)
        {
            frmLocalizarAluno loc = new frmLocalizarAluno(this);
            loc.ShowDialog();
        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            limparCampos();
            habilitarComponentes();
            txtB1.Focus();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            limparCampos();
            desabilitarComponentes();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            int id;
            float b1, b2, b3, b4;
            if (txtAlunoId.Text != "")
            {
                if (txtId.Text != "")
                {
                    id = int.Parse(txtId.Text);
                }
                else
                {
                    id = 0;
                }
                try
                {
                    if (txtB1.Text == "")
                    {
                        txtB1.Text = "0";
                    }
                    if (txtB2.Text == "")
                    {
                        txtB2.Text = "0";
                    }
                    if (txtB3.Text == "")
                    {
                        txtB3.Text = "0";
                    }
                    if (txtB4.Text == "")
                    {
                        txtB4.Text = "0";
                    }
                    b1 = float.Parse(txtB1.Text);
                    b2 = float.Parse(txtB2.Text);
                    b3 = float.Parse(txtB3.Text);
                    b4 = float.Parse(txtB4.Text);
                }
                catch (Exception error)
                {
                    MessageBox.Show("Notas inválidas", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                Aluno aluno = new Aluno(int.Parse(txtAlunoId.Text));
                aluno = AlunoDAO.procurarAlunoPorId(aluno);
                Nota nota = new Nota(id, aluno, b1, b2, b3, b4);
                if (nota.id == 0) //Inclusão
                {
                    if (NotaDAO.procurarNotaPorAluno(nota) == null)
                    {
                        if (NotaDAO.incluirNota(nota) == true)
                        {
                            nota = NotaDAO.procurarNotaPorAluno(nota);
                            mostrarNota(nota);
                            MessageBox.Show("Operação realizada com sucesso", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            desabilitarComponentes();
                        }
                        else
                        {
                            MessageBox.Show("A operação não pôde ser realizada", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        }
                    }
                    else
                    {
                        MessageBox.Show("As notas do aluno já cadastradas", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                else //Alteração
                {
                    NotaDAO.atualizarNota(nota);
                    MessageBox.Show("Operação realizada com sucesso", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    desabilitarComponentes();
                }
            }
            else
            {
                MessageBox.Show("O campo Aluno é de preenchimento obrigatório", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnLocalizar_Click(object sender, EventArgs e)
        {
            frmLocalizarAluno loc = new frmLocalizarAluno(this);
            loc.ShowDialog();
            if (txtAlunoId.Text != "")
            {
                Aluno aluno = new Aluno(int.Parse(txtAlunoId.Text));
                Nota nota = new Nota();
                nota.aluno = aluno;
                nota = NotaDAO.procurarNotaPorAluno(nota);
                if (nota != null)
                {
                    mostrarNota(nota);
                }
                else
                {
                    MessageBox.Show("Notas não cadastradas para o aluno selecionado.", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    limparCampos();
                }
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (txtId.Text != "")
            {
                habilitarComponentes();
                txtB1.Focus();
            }            
        }
    }
}
