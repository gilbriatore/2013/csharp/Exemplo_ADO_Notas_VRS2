﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace vo
{
    class Nota
    {
        public int id;
        public Aluno aluno { get; set; }
        public float b1;
        public float b2;
        public float b3;
        public float b4;

        public Nota() { }
        public Nota(Aluno aluno, float b1, float b2, float b3, float b4)
        {
            this.aluno = aluno;
            this.b1 = b1;
            this.b2 = b2;
            this.b3 = b3;
            this.b4 = b4;
        }
        public Nota(int id, Aluno aluno, float b1, float b2, float b3, float b4)
        {
            this.id = id;
            this.aluno = aluno;
            this.b1 = b1;
            this.b2 = b2;
            this.b3 = b3;
            this.b4 = b4;
        }
    }
}
