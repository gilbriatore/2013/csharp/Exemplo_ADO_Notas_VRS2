﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using util;
using vo;

namespace dao
{
    class NotaDAO
    {
        public static Nota procurarNotaPorId(Nota nota)
        {
            SqlCommand cmd = new SqlCommand();
            cmd = new SqlCommand();
            cmd.Connection = DAO.abrirBanco();
            cmd.CommandText = "SELECT * FROM Notas WHERE nt_Id = " + nota.id;
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                Aluno aluno = new Aluno();
                aluno.id = (int)dr["nt_Aluno"];
                nota.aluno = AlunoDAO.procurarAlunoPorId(aluno);
                nota.b1 = (float)dr["nt_B1"];
                nota.b2 = (float)dr["nt_B2"];
                nota.b3 = (float)dr["nt_B3"];
                nota.b4 = (float)dr["nt_B4"];
                dr.Close();
                return nota;
            }
            dr.Close();
            return null;
        }

        public static Nota procurarNotaPorAluno(Nota nota)
        {
            SqlCommand cmd = new SqlCommand();
            cmd = new SqlCommand();
            cmd.Connection = DAO.abrirBanco();
            cmd.CommandText = "SELECT * FROM Notas WHERE nt_aluno = " + nota.aluno.id;
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                nota.aluno = AlunoDAO.procurarAlunoPorId(nota.aluno);
                nota.id = (int)dr["nt_Id"];
                nota.b1 = (float)dr["nt_B1"];
                nota.b2 = (float)dr["nt_B2"];
                nota.b3 = (float)dr["nt_B3"];
                nota.b4 = (float)dr["nt_B4"];
                dr.Close();
                return nota;
            }
            dr.Close();
            return null;
        }

        public static bool incluirNota(Nota nota)
        {
            if (procurarNotaPorId(nota) == null)
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = DAO.abrirBanco();
                cmd.CommandText = "INSERT INTO Notas(nt_Aluno, nt_B1, nt_B2, nt_B3, nt_B4) VALUES (@nt_Aluno, @nt_B1, @nt_B2, @nt_B3, @nt_B4)";
                cmd.Parameters.AddWithValue("@nt_Aluno", nota.aluno.id);
                cmd.Parameters.AddWithValue("@nt_B1", nota.b1);
                cmd.Parameters.AddWithValue("@nt_B2", nota.b2);
                cmd.Parameters.AddWithValue("@nt_B3", nota.b3);
                cmd.Parameters.AddWithValue("@nt_B4", nota.b4);
                cmd.ExecuteNonQuery();
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool atualizarNota(Nota nota)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = DAO.abrirBanco();
            cmd.CommandText = "UPDATE Notas SET nt_B1 = @nt_B1, nt_B2 = @nt_B2, nt_B3 = @nt_B3, nt_B4 = @nt_B4 WHERE nt_Id = " + nota.id;
            cmd.Parameters.AddWithValue("@nt_B1", nota.b1);
            cmd.Parameters.AddWithValue("@nt_B2", nota.b2);
            cmd.Parameters.AddWithValue("@nt_B3", nota.b3);
            cmd.Parameters.AddWithValue("@nt_B4", nota.b4);
            cmd.ExecuteNonQuery();
            return true;
        }
    }
}
