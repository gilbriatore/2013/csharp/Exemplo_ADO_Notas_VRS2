﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using vo;
using util;
using System.Data;
using System.Data.SqlClient;
namespace dao
{
    class AlunoDAO
    {
        static List<Aluno> alunos = new List<Aluno>();
        public static Aluno procurarAlunoPorNome(Aluno aluno)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = DAO.abrirBanco();
            cmd.CommandText = "SELECT * FROM Alunos WHERE al_Nome = '" + aluno.nome + "'";
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                aluno.id = (int)dr["al_Id"];
                aluno.cpf = dr["al_Cpf"].ToString();
                aluno.contato = dr["al_Contato"].ToString();
                dr.Close();
                return aluno;
            }
            dr.Close();
            return null;
        }

        public static Aluno procurarAlunoPorId(Aluno aluno)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = DAO.abrirBanco();
            cmd.CommandText = "SELECT * FROM Alunos WHERE al_Id = '" + aluno.id + "'";
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                aluno.nome =  dr["al_Nome"].ToString().Trim();
                aluno.cpf = dr["al_Cpf"].ToString();
                aluno.contato = dr["al_Contato"].ToString();
                dr.Close();
                return aluno;
            }
            dr.Close();
            return null;
        }

        public static Aluno procurarAlunoPorCpf(Aluno aluno)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = DAO.abrirBanco();
            cmd.CommandText = "SELECT * FROM Alunos WHERE al_Cpf = '" + aluno.cpf + "'";
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                aluno.nome = dr["al_Nome"].ToString().Trim();
                aluno.id = (int)dr["al_Id"];                
                aluno.contato = dr["al_Contato"].ToString();
                dr.Close();
                return aluno;
            }
            dr.Close();
            return null;
        }

        public static List<Aluno> obterAlunos(Aluno aluno)
        {
            List<Aluno> alunos = new List<Aluno>();
            Aluno al;
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = DAO.abrirBanco();
            cmd.CommandText = "SELECT * FROM Alunos WHERE al_Nome LIKE '%" + aluno.nome + "%' ORDER BY al_Nome";
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                al = new Aluno();
                al.id = (int)dr["al_Id"];
                al.nome = dr["al_Nome"].ToString().Trim() ;
                al.cpf = dr["al_Cpf"].ToString() ;
                al.contato = dr["al_Contato"].ToString(); ;
                alunos.Add(al);
            }
            return alunos;
        }

        public static bool incluirAluno(Aluno aluno)
        {
            if (procurarAlunoPorNome(aluno) == null)
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = DAO.abrirBanco();
                cmd.CommandText = "INSERT INTO Alunos(al_Nome, al_Cpf, al_Contato) VALUES (@al_Nome, @al_Cpf, @al_Contato)";
                cmd.Parameters.AddWithValue("@al_Nome", aluno.nome);
                cmd.Parameters.AddWithValue("@al_Cpf", aluno.cpf);
                cmd.Parameters.AddWithValue("@al_Contato", aluno.contato);
                cmd.ExecuteNonQuery();
                return true;
            }
            else
            {
                return false;
            }            
        }

        public static bool atualizarAluno(Aluno aluno)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = DAO.abrirBanco();
            cmd.CommandText = "UPDATE Alunos SET al_Nome = @al_Nome, al_Contato = @al_Contato WHERE al_Id = " + aluno.id;
            cmd.Parameters.AddWithValue("@al_Nome", aluno.nome);
            cmd.Parameters.AddWithValue("@al_Contato", aluno.contato);
            cmd.ExecuteNonQuery();
            return true;            
        }
    }
}
